# Trending Photography Banner Concept

A Pen created on CodePen.io. Original URL: [https://codepen.io/yudizsolutions/pen/yLZaJMx](https://codepen.io/yudizsolutions/pen/yLZaJMx).

We are using HTML and CSS to create an image moving design. We're also using text typing with typescript and a basic GSAP animation for text show on page load.

We took reference from our dribble shot: 
https://dribbble.com/shots/21522767-Click-Savvy

Made by Radhika Bhadeliya from Yudiz 