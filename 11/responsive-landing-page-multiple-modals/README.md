# Responsive Landing Page & Multiple Modals

A Pen created on CodePen.io. Original URL: [https://codepen.io/ecemgo/pen/GRPBYpb](https://codepen.io/ecemgo/pen/GRPBYpb).

This pen includes CSS grid for layouts and CSS grid area is used to be responsive. On each card, the hover effect is used. When the mouse is moved over the card, the image shrinks by half. When moved out, the image returns to its previous size at the same speed. Additionally, this pen has multiple modals including a modal header, body, and footer. 

Inspirations: 
1. https://www.hover.dev/components/cards 
2. https://dribbble.com/shots/15894005-SEO-Service-I-Digital-Agency-Landing-Page 