# Very cool animated text

A Pen created on CodePen.io. Original URL: [https://codepen.io/obaidismailz/pen/LYavyXo](https://codepen.io/obaidismailz/pen/LYavyXo).

This is a very cool animation that I saw on Instagram @mewtru, and I decided to code it. Since I'm using Firefox, I fixed the animation with JavaScript as well.

Link: https://www.instagram.com/reel/C1uv6Kqv19T/?utm_source=ig_web_copy_link&igsh=MjM0N2Q2NDBjYg==