var swiper = new Swiper(".swiper", {
    effect: "cards",
    grabCursor: true,
    initialSlide: 2,
    speed: 500,
    loop: true,
    rotate: true,
    mousewheel: {
    invert: false,
  },
});

const data = [
  {
    imageUrl: "https://github.com/ecemgo/mini-samples-great-tricks/assets/13468728/b6f5eb64-887c-43b1-aaba-d52a4c59a379",
    rating: 8.5,
    title: "The Queen's Gambit"
  },
  {
    imageUrl: "https://github.com/ecemgo/mini-samples-great-tricks/assets/13468728/e906353b-fde0-4518-9a03-16545c1161bd",
    rating: 9.5,
    title: "Breaking Bad"
  },
  // Add more objects as needed
];

