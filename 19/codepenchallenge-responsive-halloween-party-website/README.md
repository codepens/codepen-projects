# CodePenChallenge: Responsive Halloween Party Website

A Pen created on CodePen.io. Original URL: [https://codepen.io/nilimadas/pen/PoVqzNg](https://codepen.io/nilimadas/pen/PoVqzNg).

This pen work relates to #codepenchallenge which features a responsive Halloween website design using HTML CSS & JavaScript.With CSS animations when scrolling and a dark and elegant interface.